package org.chj.ftda.block;

/**
 * A block defining what makes 
 * @author Jesper
 *
 */
public class BlockDefinition {
	protected String GUID;
	protected String name;
	protected String group_GUID;
	
	public String getGUID() {
		return GUID;
	}
	public void setGUID(String gUID) {
		GUID = gUID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGroupGUID() {
		return group_GUID;
	}
	public void setGroupGUID(String group_GUID) {
		this.group_GUID = group_GUID;
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
