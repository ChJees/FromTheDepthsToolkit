package org.chj.ftda.data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.chj.ftda.data.property.InterfacePropertySerializer;
import org.chj.ftda.data.property.PropertySerializer;
import org.chj.ftda.data.property.SimplePropertySerializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Properties file for this application. Can be anything from application wide settings to properties for tools.
 * @author ChJees
 *
 */
public class PropertiesFile extends AbstractDataFile {
	
	/** The array storing all serializers. Do not tamper with it directly. */
	protected final static ArrayList<PropertySerializer> serializers = new ArrayList<PropertySerializer>();
	
	/**
	 * HashMap containing all values to be modified.
	 */
	protected HashMap<String, Object> values;
	
	/**
	 * Initializes an empty properties file.
	 */
	public PropertiesFile() {
		//Initialize a empty file.
		setValues(new HashMap<String, Object>());
		
		if(serializers.isEmpty())
		{
			initSerializers();
		}
	}
	
	/**
	 * Initializes a empty properties file ready to be used. Preferred way of doing things.
	 * @param file
	 */
	public PropertiesFile(File file) {
		//Initialize a empty file.
		setValues(new HashMap<String, Object>());
		createFile(file);
		
		if(serializers.isEmpty())
		{
			initSerializers();
		}
	}

	@Override
	protected void load(File file, JsonNode root) {
		//Iterate over the entire Json structure and set relevant HashMap values with that.
		if(getRootNode() != null && getRootNode().isObject())
		{
			ObjectNode node = (ObjectNode) getRootNode();
			
			Iterator<String> it = node.fieldNames();
			while(it.hasNext())
			{
				String entry = it.next();
				loadField(entry);
			}
		}
	}
	
	@Override
	protected void save(File file, JsonNode jsonNode) {
		//Iterate over the HashMap values and save them.
		if(getRootNode() != null && !map().isEmpty() && getRootNode().isObject())
		{
			Iterator<Entry<String, Object>> it = map().entrySet().iterator();
			while(it.hasNext())
			{
				Entry<String, Object> entry = it.next();
				setField(entry.getKey(), entry.getValue());
			}
		}
	}
	
	/**
	 * Function for setting a value in the underlying Json structure.
	 * @param key Json key.
	 * @param value Value to set.
	 */
	protected void setField(String key, Object value)
	{
		if(getRootNode() != null && getRootNode().isObject())
		{
			ObjectNode node = (ObjectNode) getRootNode();
			
			for(PropertySerializer serializer : serializers)
			{
				serializer.serialize(key, map(), node, value);
			}
		}
	}
	
	/**
	 * Function for setting a value from a loaded Jsons node structure.
	 * @param key Key to load.
	 */
	protected void loadField(String key)
	{
		if(getRootNode() != null && getRootNode().isObject())
		{
			JsonNode node = getRootNode();
			
			if(node.has(key))
			{
				for(PropertySerializer serializer : serializers)
				{
					serializer.deserialize(key, map(), node);
				}
			}
		}
	}
	
	/**
	 * If you want custom data loading in your properties add your own PropertySerializer.
	 * @param e Your serializer payload.
	 * @return true if it managed to be added.
	 */
	public static boolean addPropertySerializer(PropertySerializer e) {
		return serializers.add(e);
	}
	
	/**
	 * Setting up the core serializers.
	 */
	protected static void initSerializers()
	{
		addPropertySerializer(new SimplePropertySerializer());
		addPropertySerializer(new InterfacePropertySerializer());
	}
	
	/**
	 * Get the underlying map.
	 * @return
	 */
	public HashMap<String, Object> map() {
		return values;
	}
	
	/**
	 * Override this HashMap by inserting your own. No ill effects come from it.
	 * @param values
	 */
	private void setValues(HashMap<String, Object> values) {
		this.values = values;
	}
}
