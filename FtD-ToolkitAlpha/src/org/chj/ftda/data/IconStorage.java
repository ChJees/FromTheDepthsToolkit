package org.chj.ftda.data;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class IconStorage extends AbstractStorage<Image> {
	
	public IconStorage() {
		//Stuff
	}
	
	@Override
	protected Image load(File file) {
		//Attempt to load a icon.
		try {
			Image result = ImageIO.read(file);
			
			if(result != null)
				return result;
		} catch (IOException e) {
			//Something went very wrong.
			return null;
		}
		
		return null;
	}

}
