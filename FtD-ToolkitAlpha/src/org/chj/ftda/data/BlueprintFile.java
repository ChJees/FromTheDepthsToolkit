package org.chj.ftda.data;

import java.io.File;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * From the Depths Blueprint in its raw data form.
 * @author ChJees
 *
 */
public class BlueprintFile extends AbstractDataFile{

	public BlueprintFile() {
		//Do stuff.
	}

	@Override
	protected void load(File file, JsonNode root) {
		// Load
		
	}

	@Override
	protected void save(File file, JsonNode jsonNode) {
		// Save
		
	}
}
