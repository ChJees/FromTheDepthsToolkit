package org.chj.ftda.data;

public enum DataFileState {
	SUCCESS, FAILURE, ALREADY_EXISTS, FILE_NOT_FOUND, IO_ERROR, PARSE_ERROR, EMPTY_FILE;
	
	/**
	 * Everything went smoothly.
	 * @return true on success.
	 */
	public boolean isSuccess()
	{
		return this == SUCCESS ? true : false;
	}
	
	/**
	 * Something went wrong. Not descriptive of what though.
	 * @return true if the operation was a failure.
	 */
	public boolean isFailure()
	{
		return this != SUCCESS ? true : false;
	}
}
