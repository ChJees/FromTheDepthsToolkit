package org.chj.ftda.data;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Abstraction of the gritty and boring internals of a Json data file. 
 * Do the usual error checking and opening and closing of a file.
 * @author ChJees
 *
 */
public abstract class AbstractDataFile {
	/**
	 * File that was used by this Data File.
	 */
	private File data_file;
	/**
	 * Root node of the loaded Json file. Up to the data file to make further sense of the contents.
	 */
	protected JsonNode data;
	
	/**
	 * Last state of this data file set.
	 */
	protected DataFileState last_file_state;
	
	/**
	 * Determines what policy to use with this file during saving. Default: DataFilePolicy.OVERWRITE
	 */
	protected DataFilePolicy data_policy;
	
	/**
	 * Output Json in a pretty easily human readable way. E.g:<p>
	 * {<br/>
	 * "Core.Window.Position" : [ "Point", 240, 194 ],<br/>
	 * "Core.Window.State" : 0,<br/>
	 * "Core.Window.Size" : [ "Dimension", 748, 472 ]<br/>
	 * }</p>
	 */
	protected boolean pretty_output;
	
	/**
	 * Abstraction of the gritty internals of a Json data file. 
	 * Do the usual error checking and opening and closing of a file.
	 */
	public AbstractDataFile() {
		//Initialize variables
		setDataFile(null);
		setRootNode(null);
		setLastFileState(DataFileState.EMPTY_FILE);
		setDataPolicy(DataFilePolicy.OVERWRITE);
		setPrettyOutput(false);
	}
	
	/**
	 * Implementation of loading specific to the Json file type.
	 * @param file File being loaded. For completeness sake.
	 * @param root Root node of loaded file.
	 */
	protected abstract void load(File file, JsonNode root);
	
	/**
	 * Implementation of saving specific to the Json file type. I.e update the nodes with new information.
	 * @param file File being saved. For completeness sake.
	 * @param jsonNode Node to be saved.
	 */
	protected abstract void save(File file, JsonNode jsonNode);
	
	/**
	 * Resets this files node\Creates a node to edit.
	 */
	public void createFile(File file)
	{
		if(file != null)
		{
			setRootNode(JsonGen.getObjectMapper().createObjectNode());
			setDataFile(file);
		}
	}
	
	/**
	 * Loads a file off the disk. Taking care of all the mundane operations leaving you to the actual implementation.
	 * @param file File to load.
	 * @return SUCCESS if the operation went through perfectly. Otherwise FAILURE or more descriptive error states.
	 */
	public DataFileState loadFile(File file)
	{
		if(file == null)
		{
			setLastFileState(DataFileState.FAILURE);
			return DataFileState.FAILURE;
		}
		
		if(file.exists())
		{
			try {
				JsonNode root = JsonGen.getObjectMapper().readTree(file);
				if(getRootNode() == null)
				{
					setLastFileState(DataFileState.EMPTY_FILE);
					return DataFileState.EMPTY_FILE;
				}
				
				setDataFile(file);
				
				setRootNode(root);
				
				setLastFileState(DataFileState.SUCCESS);
				
				load(file, root);
				
				return DataFileState.SUCCESS;
			} catch (JsonProcessingException e) {
				//Failed to properly parse the Json file.
				setLastFileState(DataFileState.PARSE_ERROR);
				return DataFileState.PARSE_ERROR;
			} catch (IOException e) {
				//Low level Input\Output error.
				setLastFileState(DataFileState.IO_ERROR);
				return DataFileState.IO_ERROR;
			}
		}else{
			setLastFileState(DataFileState.FILE_NOT_FOUND);
			return DataFileState.FILE_NOT_FOUND;
		}
	}
	
	/**
	 * Loads a file off the disk using last set file. Taking care of all the mundane operations leaving you to the actual implementation.
	 * @return SUCCESS if the operation went through perfectly. Otherwise FAILURE or more descriptive error states.
	 */
	public DataFileState loadFile()
	{
		return loadFile(getDataFile());
	}
	
	/**
	 * Save a file off the disk. Taking care of all the mundane operations leaving you to the actual implementation.
	 * @param file File to load.
	 * @return SUCCESS if the operation went through perfectly. Otherwise FAILURE or more descriptive error states.
	 */
	public DataFileState saveFile(File file)
	{
		if(file == null)
		{
			setLastFileState(DataFileState.FAILURE);
			return DataFileState.FAILURE;
		}
		
		if(!file.exists())
		{
			try {
				file.createNewFile();
			} catch (IOException e) {
				setLastFileState(DataFileState.IO_ERROR);
				return DataFileState.IO_ERROR;
			}
		}
		
		if(file.exists() && !getDataPolicy().canOverwrite())
		{
			setLastFileState(DataFileState.ALREADY_EXISTS);
			return DataFileState.ALREADY_EXISTS;
		}
		
		if(getRootNode() == null)
		{
			setLastFileState(DataFileState.FAILURE);
			return DataFileState.FAILURE;
		}
		
		setDataFile(file);
		
		save(file, getRootNode());
		
		try {
			if(usePrettyOutput())
			{
				JsonGenerator generator = JsonGen.getObjectMapper().getFactory().createGenerator(getDataFile(), JsonEncoding.UTF8);
				generator.useDefaultPrettyPrinter();
				
				JsonGen.getObjectMapper().writeTree(
						generator,
						getRootNode()
						);
			}else{
				JsonGen.getObjectMapper().writeTree(
						JsonGen.getObjectMapper().getFactory().createGenerator(getDataFile(), JsonEncoding.UTF8),
						getRootNode()
						);
			}
			
			setLastFileState(DataFileState.SUCCESS);
			return DataFileState.SUCCESS;
		} catch (JsonProcessingException e) {
			//Failed to properly write the Json file.
			setLastFileState(DataFileState.PARSE_ERROR);
			return DataFileState.PARSE_ERROR;
		} catch (IOException e) {
			//Low level Input\Output error.
			setLastFileState(DataFileState.IO_ERROR);
			return DataFileState.IO_ERROR;
		}
	}
	
	/**
	 * Save a file off the disk using last set file. Taking care of all the mundane operations leaving you to the actual implementation.
	 * @return SUCCESS if the operation went through perfectly. Otherwise FAILURE or more descriptive error states.
	 */
	public DataFileState saveFile()
	{
		return saveFile(getDataFile());
	}
	
	/**
	 * Get the last used file used for saving and\or loading.
	 * @return File used.
	 */
	public File getDataFile() {
		return data_file;
	}
	
	/**
	 * Sets the last used file. Internal use only.
	 * @param data_file Last used data file.
	 */
	private void setDataFile(File data_file) {
		this.data_file = data_file;
	}
	
	/**
	 * Get the Root node of this data file.
	 * @return Root node.
	 */
	public JsonNode getRootNode() {
		return data;
	}
	
	/**
	 * Sets the root node of the data file. Internal use only.
	 * @param root_node New root node.
	 */
	private void setRootNode(JsonNode root_node) {
		this.data = root_node;
	}
	
	/**
	 * Get the last state of this data file.
	 * @return Last state.
	 */
	public DataFileState getLastFileState() {
		return last_file_state;
	}
	
	/**
	 * Sets the last state that set on this data file. Internal use only.
	 * @param last_file_state
	 */
	private void setLastFileState(DataFileState last_file_state) {
		this.last_file_state = last_file_state;
	}
	
	/**
	 * Get the currently used data policy.
	 * @return Current data policy.
	 */
	public DataFilePolicy getDataPolicy() {
		return data_policy;
	}
	
	/**
	 * Sets the policy to use when saving.
	 * @param data_policy Data policy.
	 */
	public void setDataPolicy(DataFilePolicy data_policy) {
		this.data_policy = data_policy;
	}
	
	/**
	 * Does it use pretty output when saving?
	 * @return true if using pretty output.
	 */
	public boolean usePrettyOutput() {
		return pretty_output;
	}

	/**
	 * Set whether to use pretty output or not. Adds newlines and indents if true.
	 * @param pretty_output New pretty output state.
	 */
	public void setPrettyOutput(boolean pretty_output) {
		this.pretty_output = pretty_output;
	}
}
