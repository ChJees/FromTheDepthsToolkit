package org.chj.ftda.data;

/**
 * The policy when saving a data file. Determines whether it cares if it overwrites the file or not.
 * @author Jesper
 *
 */
public enum DataFilePolicy {
	OVERWRITE, DO_NOT_OVERWRITE;
	
	public boolean canOverwrite()
	{
		return this == OVERWRITE ? true : false;
	}
}
