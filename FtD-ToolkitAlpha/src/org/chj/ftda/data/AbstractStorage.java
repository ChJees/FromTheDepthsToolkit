package org.chj.ftda.data;

import java.io.File;
import java.util.HashMap;

/**
 * A class that is a abstract storage of data. 
 * Not as a specialized as the AbstractDataFile 
 * due to data types like icons and images are
 * easier to handle.
 * 
 * @author ChJees
 *
 * @param <V> Data type
 */
public abstract class AbstractStorage<V> {
	
	/**
	 * Map storing all loaded values.
	 */
	protected HashMap<String, V> values;
	
	/**
	 * Root directory of where to look from.
	 */
	protected File root_directory;
	
	/**
	 * Should it recursively search through directories to find the file? (Default=false)
	 */
	protected boolean recursive_search;
	
	/**
	 * A empty storage. Make sure nothing is null.
	 */
	public AbstractStorage() {
		values = new HashMap<String, V>();
		setRootDirectory(new File(""));
		setRecursiveSearchMode(false);
	}
	
	/**
	 * Prefrerred way of initialising storage. Sets the the root directory.
	 * @param root Root directory to set.
	 */
	public AbstractStorage(File root) {
		values = new HashMap<String, V>();
		setRootDirectory(root);
		setRecursiveSearchMode(false);
	}
	
	/**
	 * Try to get the key first. If not try to load it through implementation specific method.
	 * @param key Key\Filename to look for.
	 * @return Object if successful, null if failed to get\load.
	 */
	public V get(String key)
	{
		if(values.containsKey(key))
		{
			return values.get(key);
		}else{
			V result = load(new File(getRootDirectory().getAbsolutePath() + File.separatorChar + key));
			if(result != null)
			{
				values.put(key, result);
				return result;
			}
			
			//Recursive search.
			if(result == null && isSearchingRecursively())
			{
				return recurse(key, getRootDirectory());
			}
		}
		
		return null;
	}
	
	/**
	 * Directory recursion function. Will look for and through directories 
	 * and try to find a file specified by the key.
	 * @param key Filename key to look for.
	 * @param file Current directory.
	 * @return Loaded value if found otherwise null.
	 */
	private V recurse(String key, File file)
	{
		//First try to find related key.
		File result = new File(file.getAbsolutePath() + File.separatorChar + key);
		
		if(result.exists())
		{
			return load(result);
		}
		
		//Not found? Look through other directories.
		File[] file_list = file.listFiles();
		if(file_list != null)
		{
			for(File node : file_list)
			{
				if(node.isDirectory())
				{
					V out = recurse(key, node);
					if(out != null)
						return out;
				}
			}
		}
		
		//If nothing found return null.
		return null;
	}
	
	/**
	 * Attempt to load the file. Implementation specific information goes here.
	 * @param file
	 * @return Object if successful, null if failed to load.
	 */
	protected abstract V load(File file);
	
	/**
	 * Clears the loaded list.
	 */
	public void clear()
	{
		values.clear();
	}
	
	/**
	 * Get the starting directory.
	 * @return The starting directory.
	 */
	public File getRootDirectory() {
		return root_directory;
	}
	
	/**
	 * Set the starting directory to look for files.
	 * @param root_directory New root directory.
	 */
	public void setRootDirectory(File root_directory) {
		this.root_directory = root_directory;
	}
	
	/**
	 * Is it in a mode to search recursively through directories?
	 * @return true if turned on.
	 */
	public boolean isSearchingRecursively() {
		return recursive_search;
	}
	
	/**
	 * Enable or disable recursive search mod.
	 * @param recursive_search New state.
	 */
	public void setRecursiveSearchMode(boolean recursive_search) {
		this.recursive_search = recursive_search;
	}
}
