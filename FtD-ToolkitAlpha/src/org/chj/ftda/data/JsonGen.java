package org.chj.ftda.data;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class sole purpose is for easy retrieval of Jackson's Json Factory and Generator classes.
 * @author ChJees
 *
 */
public class JsonGen {
	protected static final ObjectMapper mapper = new ObjectMapper();
	
	protected static final JsonFactory factory = new JsonFactory();
	
	public static ObjectMapper getObjectMapper() {
		return mapper;
	}

	public static JsonFactory getJsonFactory() {
		return factory;
	}
	
	public JsonGen() {
		//NOOP - Static class
	}
}
