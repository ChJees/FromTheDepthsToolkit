package org.chj.ftda.data.property;

import java.util.HashMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

/**
 * Serializes and deserializes simple types in Json.
 * @author ChJees
 *
 */
public class SimplePropertySerializer implements PropertySerializer {
	
	/**
	 * Serializes and deserializes simple types in Json.
	 */
	public SimplePropertySerializer() {
		//NOOP
	}

	@Override
	public void serialize(String key, HashMap<String, Object> map, ObjectNode node, Object value) {
		//Integer
		if(value instanceof Integer)
		{
			node.set(key, new IntNode((Integer) value));
			return;
		}
		
		//Boolean
		if(value instanceof Boolean)
		{
			node.set(key, BooleanNode.valueOf((Boolean) value));
			return;
		}
		
		//String
		if(value instanceof String)
		{
			node.set(key, new TextNode((String) value));
			return;
		}
	}

	@Override
	public void deserialize(String key, HashMap<String, Object> map, JsonNode node) {
		//Integer
		if(node.get(key).isInt())
		{
			map.put(key, node.get(key).asInt());
		}
		
		//Boolean
		if(node.get(key).isBoolean())
		{
			map.put(key, node.get(key).asBoolean());
		}
		
		//String
		if(node.get(key).isTextual())
		{
			map.put(key, node.get(key).asText());
		}
	}
}
