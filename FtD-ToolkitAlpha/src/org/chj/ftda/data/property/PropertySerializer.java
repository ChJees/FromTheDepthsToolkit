package org.chj.ftda.data.property;

import java.util.HashMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Interface for dynamically let PropertiesFile read and write different values.
 * @author ChJees
 *
 */
public interface PropertySerializer {
	/**
	 * Serialize valid data types.
	 * @param key Json key name.
	 * @param map HashMap to pull data from.
	 * @param node Node to write to.
	 * @param value Value to verify and set.
	 */
	public void serialize(String key, HashMap<String, Object> map, ObjectNode node, Object value);
	
	/**
	 * Deserialize valid data types.
	 * @param key Json key name.
	 * @param map HashMap to put data into.
	 * @param node Node to read from.
	 */
	public void deserialize(String key, HashMap<String, Object> map, JsonNode node);
}
