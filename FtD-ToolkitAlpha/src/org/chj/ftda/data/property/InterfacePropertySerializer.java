package org.chj.ftda.data.property;

import java.awt.Dimension;
import java.awt.Point;
import java.util.HashMap;

import org.chj.ftda.data.JsonGen;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Serializes and deserializes compound interface oriented types in Json.
 * @author ChJees
 *
 */
public class InterfacePropertySerializer implements PropertySerializer {
	
	/**
	 * Serializes and deserializes compound interface oriented types in Json.
	 */
	public InterfacePropertySerializer() {
		//NOOP
	}

	@Override
	public void serialize(String key, HashMap<String, Object> map, ObjectNode node, Object value) {
		//Dimension
		if(value instanceof Dimension)
		{
			Dimension obj = (Dimension) value;
			ArrayNode dimension = JsonGen.getObjectMapper().createArrayNode();
			dimension.add("Dimension");
			dimension.add((int)obj.getWidth());
			dimension.add((int)obj.getHeight());
			
			node.set(key, dimension);
			return;
		}
		
		//Point
		if(value instanceof Point)
		{
			Point obj = (Point) value;
			ArrayNode dimension = JsonGen.getObjectMapper().createArrayNode();
			dimension.add("Point");
			dimension.add((int)obj.getX());
			dimension.add((int)obj.getY());
			
			node.set(key, dimension);
			return;
		}
	}

	@Override
	public void deserialize(String key, HashMap<String, Object> map, JsonNode node) {
		//Dimension
		if(node.get(key).isArray())
		{
			ArrayNode array = (ArrayNode) node.get(key);
			if(array.size() >= 3 && array.get(0).asText().compareToIgnoreCase("Dimension") == 0)
			{
				if(array.get(1).isInt() && array.get(2).isInt())
				{
					Dimension dimension = new Dimension(array.get(1).asInt(), array.get(2).asInt());
					map.put(key, dimension);
				}
			}
		}
		
		//Point
		if(node.get(key).isArray())
		{
			ArrayNode array = (ArrayNode) node.get(key);
			if(array.size() >= 3 && array.get(0).asText().compareToIgnoreCase("Point") == 0)
			{
				if(array.get(1).isInt() && array.get(2).isInt())
				{
					Point point = new Point(array.get(1).asInt(), array.get(2).asInt());
					map.put(key, point);
				}
			}
		}
	}

}
