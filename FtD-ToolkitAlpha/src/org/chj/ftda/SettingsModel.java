package org.chj.ftda;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;

import org.chj.ftda.data.PropertiesFile;

/**
 * Applications wide settings.
 * @author ChJees
 *
 */
public class SettingsModel {
	
	protected PropertiesFile file;
	
	protected Point core_window_position;
	protected Dimension core_window_size;
	protected int core_window_state;
	
	public SettingsModel() {
		file = new PropertiesFile(new File("settings.json"));
		file.setPrettyOutput(true);
	}
	
	public void save()
	{
		//Save settings
		file.map().put("Core.Window.Position", getCoreWindowPosition());
		file.map().put("Core.Window.Size", getCoreWindowSize());
		file.map().put("Core.Window.State", getCoreWindowState());
		
		file.saveFile();
	}
	
	public void load()
	{
		//Load settings
		file.loadFile();
		
		//Loaded Settings
		{
			Object setting = file.map().get("Core.Window.Position");
			if(setting != null && setting instanceof Point)
			{
				setCoreWindowPosition((Point) setting);
			}
		}
		
		{
			Object setting = file.map().get("Core.Window.Size");
			if(setting != null && setting instanceof Dimension)
			{
				setCoreWindowSize((Dimension) setting);
			}
		}
		
		{
			Object setting = file.map().get("Core.Window.State");
			if(setting != null && setting instanceof Integer)
			{
				setCoreWindowState((int) setting);
			}
		}
	}
	
	public void set(String key, Object value)
	{
		file.map().put(key, value);
	}
	
	public Object get(String key)
	{
		return file.map().get(key);
	}
	
	public boolean has(String key)
	{
		return file.map().containsKey(key);
	}

	public Point getCoreWindowPosition() {
		return core_window_position;
	}

	public void setCoreWindowPosition(Point core_window_position) {
		this.core_window_position = core_window_position;
	}

	public Dimension getCoreWindowSize() {
		return core_window_size;
	}

	public void setCoreWindowSize(Dimension core_window_size) {
		this.core_window_size = core_window_size;
	}

	public int getCoreWindowState() {
		return core_window_state;
	}

	public void setCoreWindowState(int core_window_state) {
		this.core_window_state = core_window_state;
	}
}
