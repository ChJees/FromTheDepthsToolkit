package org.chj.ftda;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.net.URISyntaxException;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.chj.ftda.data.IconStorage;
import org.chj.ftda.ui.BlockLibrary;
import org.chj.ftda.ui.ToolkitWindow;

public class ToolkitAlpha implements WindowListener {
	public ToolkitWindow main_frame;
	public boolean main_frame_maximized;
	
	protected static final SettingsModel settings = new SettingsModel();
	protected static final IconStorage icons = new IconStorage();
	
	public ToolkitAlpha() {
		//Initialize storage
		try {
			icons.setRootDirectory(new File(getClass().getResource("./res").toURI()));
			icons.setRecursiveSearchMode(true);
		} catch (URISyntaxException e1) {
			//Not supposed to happen but we can live without images.
			//System.out.println("Oops!");
		}
		
		//Set look and feel. (If possible)
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		//Initialize GUI
		main_frame = new ToolkitWindow("From the Depths: Toolkit");
		main_frame.addWindowListener(this);
		
		//Load settings.
		settings.load();
		
		main_frame.setMinimumSize(new Dimension(640, 480));
		if(settings.getCoreWindowSize() != null)
			main_frame.setSize(settings.getCoreWindowSize());
		
		if(settings.getCoreWindowPosition() != null)
			main_frame.setLocation(settings.getCoreWindowPosition());
		else
			main_frame.setLocationRelativeTo(null);
		
		main_frame.setExtendedState(settings.getCoreWindowState());
		
		{
			
			BlockLibrary frame_block_library = new BlockLibrary();
			main_frame.setFrameBlockLibrary(frame_block_library);
			
			frame_block_library.setVisible(true);
			
			{
				String key = "Core.Blocklibrary.Position";
				Object value = settings.get(key);
				if(value != null && value instanceof Point)
				{
					Point location = (Point) settings.get(key);
					frame_block_library.setLocation(location);
				}
			}
			
			{
				String key = "Core.Blocklibrary.Size";
				Object value = settings.get(key);
				if(value != null && value instanceof Dimension)
				{
					Dimension size = (Dimension) settings.get(key);
					frame_block_library.setSize(size);
				}else{
					frame_block_library.setLocationRelativeTo(main_frame);
				}
			}
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new ToolkitAlpha();
			}
		});
	}

	public static SettingsModel getSettings() {
		return settings;
	}

	public static IconStorage getIcons() {
		return icons;
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
		//Exit if it is the main window that closed.
		if(e.getWindow() == main_frame)
		{
			//Save settings
			settings.save();
			
			//Exit
			System.exit(0);
		}
	}

	@Override
	public void windowClosing(WindowEvent e) {
		//Cancel closing if there is any unsaved blueprint or edit open.
		if(e.getWindow() == main_frame)
		{
			//Set new settings.
			settings.setCoreWindowPosition(main_frame.getLocation());
			settings.setCoreWindowSize(main_frame.getSize());
			settings.setCoreWindowState(main_frame.getExtendedState());
			settings.set("Core.Blocklibrary.Position", main_frame.getFrameBlockLibrary().getLocation());
			settings.set("Core.Blocklibrary.Size", main_frame.getFrameBlockLibrary().getSize());
			
			//Check any handlers for changes.
			
			//Everything's dandy. Proceed with closing.
			e.getWindow().dispose();
		}
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}
}
