package org.chj.ftda.ui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JDesktopPane;

public class LogoDesktopPane extends JDesktopPane {

	private static final long serialVersionUID = -5086726191960915642L;
	
	protected Image background_image;
	
	public LogoDesktopPane() {
		//Meh. Just a fancier JDeskTopPane
	}
	
	
	@Override
	protected void paintComponent(Graphics g) {
		//Do usual painting
		super.paintComponent(g);
		
		//Paint this with the a snazzy logo in the background.
		if(g != null && getBackgroundImage() != null)
		{
			Graphics2D g2 = (Graphics2D) g;
			int width = getBackgroundImage().getWidth(null) / 2;
			int height = getBackgroundImage().getHeight(null) / 2;
			g2.drawImage(getBackgroundImage(), (getWidth() / 2) - width, (getHeight() / 2) - height, null);
		}
	}


	public Image getBackgroundImage() {
		return background_image;
	}


	public void setBackgroundImage(Image background_image) {
		this.background_image = background_image;
	}
}
