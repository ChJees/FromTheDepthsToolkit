package org.chj.ftda.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.border.BevelBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import org.chj.ftda.ToolkitAlpha;

import com.jogamp.newt.event.InputEvent;
import com.jogamp.newt.event.KeyEvent;

public class ToolkitWindow extends JFrame implements InternalFrameListener, ComponentListener, ActionListener {

	private static final long serialVersionUID = 5031076843877668360L;
	
	protected LogoDesktopPane desktop_panel;
	
	protected JInternalFrame active_internal_frame;
	
	//Save\Load System, Menu
	/** A list over all components which are enabled and depending on if there are any frames open with the capability to save. */
	protected ArrayList<Component> save_sensitive_components;
	/** A list over all components which controls closing of frames. */
	protected ArrayList<Component> closeable_components;
	
	//Window System, Menu
	protected JMenu menu_window;
	protected JMenuItem menu_indicator_item;
	protected HashMap<JInternalFrame, JMenuItem> menu_window_items;
	
	//Statusbar
	protected JPanel status_bar;
	
	//Block Library
	protected BlockLibrary frame_block_library;
	

	public ToolkitWindow() throws HeadlessException {
		init();
	}

	public ToolkitWindow(GraphicsConfiguration arg0) {
		super(arg0);
		init();
	}

	public ToolkitWindow(String arg0) throws HeadlessException {
		super(arg0);
		init();
	}

	public ToolkitWindow(String arg0, GraphicsConfiguration arg1) {
		super(arg0, arg1);
		init();
	}
	
	/**
	 * Initialize the window.
	 */
	private void init()
	{
		//Initialize values
		menu_window_items = new HashMap<JInternalFrame, JMenuItem>();
		save_sensitive_components = new ArrayList<Component>();
		closeable_components = new ArrayList<Component>();
		
		//Set defaults.
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		//Add controls
		populate();
		
		//Set icon.
		Image icon_small = ToolkitAlpha.getIcons().get("icon_small.png");
		Image icon = ToolkitAlpha.getIcons().get("icon.png");
		setIconImages(Arrays.asList(icon_small, icon));
		
		//Make visible and pack.
		setVisible(true);
		pack();
	}
	
	/**
	 * Add controls.
	 */
	private void populate()
	{
		//Add menu bar
		{
			JMenuBar menubar = new JMenuBar();
			
			//File Menu
			{
				JMenu menu = new JMenu("File");
				
				{
					JMenuItem item = new JMenuItem("Open");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("folder.png")));
					item.setMnemonic(KeyEvent.VK_O);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
					menu.add(item);
				}
				
				{
					JMenuItem item = new JMenuItem("Save");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("disk.png")));
					item.setMnemonic(KeyEvent.VK_S);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
					item.setActionCommand("FTDT_FRAME_SAVE");
					item.addActionListener(this);
					menu.add(item);
					save_sensitive_components.add(item);
				}
				
				{
					JMenuItem item = new JMenuItem("Save As");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("disk_as.png")));
					item.setMnemonic(KeyEvent.VK_A);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
					item.setActionCommand("FTDT_FRAME_SAVE_AS");
					item.addActionListener(this);
					menu.add(item);
					save_sensitive_components.add(item);
				}
				
				{
					JMenuItem item = new JMenuItem("Save All");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("disk_multiple.png")));
					item.setMnemonic(KeyEvent.VK_V);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
					item.setActionCommand("FTDT_FRAME_SAVE_ALL");
					item.addActionListener(this);
					menu.add(item);
					save_sensitive_components.add(item);
				}
				
				menu.add(new JSeparator());
				
				{
					JMenuItem item = new JMenuItem("Close");
					item.setMnemonic(KeyEvent.VK_L);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
					item.setActionCommand("FTDT_FRAME_CLOSE");
					item.addActionListener(this);
					menu.add(item);
					closeable_components.add(item);
				}
				
				{
					JMenuItem item = new JMenuItem("Close All");
					item.setMnemonic(KeyEvent.VK_E);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
					item.setActionCommand("FTDT_FRAME_CLOSE_ALL");
					item.addActionListener(this);
					menu.add(item);
					closeable_components.add(item);
				}
				
				menu.add(new JSeparator());
				
				{
					JMenuItem item = new JMenuItem("Exit");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("door_open.png")));
					item.setMnemonic(KeyEvent.VK_X);
					item.setActionCommand("FTDT_EXIT");
					item.addActionListener(this);
					menu.add(item);
				}
				
				menubar.add(menu);
			}
			
			//Edit Menu
			{
				JMenu menu = new JMenu("Edit");
				
				{
					JMenuItem item = new JMenuItem("Undo");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("arrow_undo.png")));
					item.setMnemonic(KeyEvent.VK_Z);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK));
					item.setActionCommand("FTDT_EDIT_UNDO");
					item.addActionListener(this);
					
					item.setEnabled(false); //For now
					menu.add(item);
				}
				
				{
					JMenuItem item = new JMenuItem("Redo");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("arrow_redo.png")));
					item.setMnemonic(KeyEvent.VK_R);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_MASK));
					item.setActionCommand("FTDT_EDIT_REDO");
					item.addActionListener(this);
					
					item.setEnabled(false); //For now
					menu.add(item);
				}
				
				menu.add(new JSeparator());
				
				{
					JMenuItem item = new JMenuItem("Clone");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("bullet_blue_clone.png")));
					item.setMnemonic(KeyEvent.VK_N);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
					item.setActionCommand("FTDT_EDIT_CLONE");
					item.addActionListener(this);
					menu.add(item);
				}
				
				{
					JMenuItem item = new JMenuItem("Delete");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("cross.png")));
					item.setMnemonic(KeyEvent.VK_D);
					//item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
					item.setActionCommand("FTDT_EDIT_DELETE");
					item.addActionListener(this);
					menu.add(item);
				}
				
				menubar.add(menu);
			}
			
			//View Menu
			{
				JMenu menu = new JMenu("View");
				
				{
					JMenuItem item = new JMenuItem("Settings");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("cog.png")));
					item.setMnemonic(KeyEvent.VK_G);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
					item.setActionCommand("FTDT_VIEW_SETTINGS");
					item.addActionListener(this);
					menu.add(item);
				}
				
				{
					JMenuItem item = new JMenuItem("Block Library");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("brick.png")));
					item.setMnemonic(KeyEvent.VK_B);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK));
					item.setActionCommand("FTDT_VIEW_BLOCKBROWSER");
					item.addActionListener(this);
					menu.add(item);
				}
				
				menu.add(new JSeparator());
				
				{
					JMenuItem item = new JMenuItem("Blueprint Tools");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("script_edit.png")));
					item.setMnemonic(KeyEvent.VK_E);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
					item.setActionCommand("FTDT_VIEW_BLUEPRINTTOOLS");
					item.addActionListener(this);
					menu.add(item);
				}
				
				menubar.add(menu);
			}
			
			//Window Menu
			{
				JMenu menu = new JMenu("Window");
				setMenuWindow(menu);
				
				{
					JMenuItem item = new JMenuItem("Hide All");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("application_hide.png")));
					item.setMnemonic(KeyEvent.VK_H);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_MASK));
					item.setActionCommand("FTDT_FRAME_HIDE_ALL");
					item.addActionListener(this);
					menu.add(item);
				}
				
				{
					JMenuItem item = new JMenuItem("Side by Side");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("application_tile_horizontal.png")));
					item.setMnemonic(KeyEvent.VK_I);
					item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
					item.setActionCommand("FTDT_FRAME_SIDE_BY_SIDE");
					item.addActionListener(this);
					menu.add(item);
				}
				
				menu.add(new JSeparator());
				
				menubar.add(menu);
			}
			
			//Help Menu
			{
				JMenu menu = new JMenu("Help");
				
				{
					JMenuItem item = new JMenuItem("Topics");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("help.png")));
					item.setActionCommand("FTDT_HELP_TOPICS");
					item.addActionListener(this);
					menu.add(item);
				}
				
				{
					JMenuItem item = new JMenuItem("Forum Thread");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("house_link.png")));
					item.setActionCommand("FTDT_HELP_FORUM");
					item.addActionListener(this);
					menu.add(item);
				}
				
				menu.add(new JSeparator());
				
				{
					JMenuItem item = new JMenuItem("About");
					item.setIcon(new ImageIcon(ToolkitAlpha.getIcons().get("information.png")));
					item.setActionCommand("FTDT_HELP_ABOUT");
					item.addActionListener(this);
					menu.add(item);
				}
				
				menubar.add(menu);
			}
			
			setJMenuBar(menubar);
		}
		
		//Add desktop
		{
			//Desktop area
			setDesktopPanel(new LogoDesktopPane());
			getDesktopPanel().setBackgroundImage(ToolkitAlpha.getIcons().get("icon.png"));
			getContentPane().add(getDesktopPanel(), BorderLayout.CENTER);
			
			//Status bar
			JPanel status_bar = new JPanel(new FlowLayout(FlowLayout.LEFT));
			status_bar.setMinimumSize(new Dimension(24, 24));
			status_bar.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			status_bar.add(new JLabel("Work In Progress Statusbar"));
			setStatusBar(status_bar);
			getContentPane().add(status_bar, BorderLayout.PAGE_END);
			
			refreshWindowStatus();
		}
	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		setActiveInternalFrame(e.getInternalFrame());
	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		//Internal frame finally closed.
		refreshWindowStatus();
	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {
		if(getMenuWindowItems().containsKey(e.getInternalFrame()))
		{
			//One of ours. Attempt to remove it from opened windows.
			if(e.getInternalFrame() instanceof AbstractFtDInternalFrame)
			{
				AbstractFtDInternalFrame iframe = (AbstractFtDInternalFrame) e.getInternalFrame();
				//Can close?
				if(iframe.canClose(this))
				{
					//Can close. Attempt to close.
					if(iframe.attemptToClose(this))
					{
						iframe.frameClosing(this);
						
						removeWindowMenuItem(iframe);
						
						iframe.dispose();
					}
				}
			}
		}else{
			//Close it anyway even though we do not handle it.
			e.getInternalFrame().dispose();
		}
	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		
	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		
	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		if(getMenuWindowItems().containsKey(e.getInternalFrame()))
		{
			e.getInternalFrame().setVisible(false);
		}
	}

	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		/* Process actions
		FTDT_FRAME_SAVE
		FTDT_FRAME_SAVE_AS
		FTDT_FRAME_SAVE_ALL
		FTDT_FRAME_CLOSE
		FTDT_FRAME_CLOSE_ALL
		FTDT_EXIT

		FTDT_EDIT_UNDO
		FTDT_EDIT_REDO
		FTDT_EDIT_DELETE
		FTDT_EDIT_CLONE

		FTDT_VIEW_BLOCKBROWSER
		FTDT_VIEW_BLUEPRINTTOOLS

		FTDT_FRAME_HIDE_ALL
		FTDT_FRAME_SIDE_BY_SIDE

		FTDT_HELP_TOPICS
		FTDT_HELP_FORUM
		FTDT_HELP_ABOUT
		*/
		
		/*
		 * Menu action event handling.
		 */
		/*
		 * File
		 */
		if(e.getActionCommand().compareTo("FTDT_FRAME_CLOSE") == 0)
		{
			//Attempt to close current active window if we can.
			if(getActiveInternalFrame() != null)
			{
				if(getActiveInternalFrame() instanceof AbstractFtDInternalFrame)
				{
					//Dispatch a event to signal it is attempting to close.
					AbstractFtDInternalFrame iframe = (AbstractFtDInternalFrame) getActiveInternalFrame();
					try {
						iframe.setClosed(true);
					} catch (PropertyVetoException e1) {
						//NOOP
					}
				}
			}
			return;
		}
		
		if(e.getActionCommand().compareTo("FTDT_FRAME_CLOSE_ALL") == 0)
		{
			//Attempt to close all windows if we can.
			if(getDesktopPanel() != null)
			{
				for(JInternalFrame frame : getDesktopPanel().getAllFrames())
				{
					if(frame instanceof AbstractFtDInternalFrame)
					{
						//Dispatch a event to signal it is attempting to close.
						AbstractFtDInternalFrame iframe = (AbstractFtDInternalFrame) frame;
						try {
							iframe.setClosed(true);
						} catch (PropertyVetoException e1) {
							//NOOP
						}
					}
				}
			}
			return;
		}
		
		if(e.getActionCommand().compareTo("FTDT_EXIT") == 0)
		{
			//Attempt to exit program by signaling all listeners.
			// TODO Add saving logic later.
			dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
			return;
		}
		
		/*
		 * View
		 */
		
		if(e.getActionCommand().compareTo("FTDT_VIEW_BLOCKBROWSER") == 0)
		{
			//Attempt to exit program by signaling all listeners.
			// TODO Add saving logic later.
			if(getFrameBlockLibrary() != null)
				getFrameBlockLibrary().setVisible(true);
			
			return;
		}
		
		/*
		 * Help
		 */
		if(e.getActionCommand().compareTo("FTDT_HELP_FORUM") == 0)
		{
			//Attempt to exit program by signaling all listeners.
			Desktop desktop = Desktop.getDesktop();
			if(desktop != null)
			{
				try {
					desktop.browse(URI.create("http://www.fromthedepthsgame.com/forum/showthread.php?tid=17595"));
				} catch (IOException e1) {
					//Tough luck, do nothing.
				}
			}
			
			return;
		}
		
		/*
		 * Windowing
		 */
		if(e.getActionCommand().compareTo("FTDT_FRAME_HIDE_ALL") == 0)
		{
			//Iconify all frames.
			for(JInternalFrame frame : getDesktopPanel().getAllFrames())
			{
				if(frame.isVisible())
					try {
						frame.setIcon(true);
					} catch (PropertyVetoException e1) {
						//NOOP
					}
			}
			
			return;
		}
		
		if(e.getActionCommand().compareTo("FTDT_FRAME_SIDE_BY_SIDE") == 0)
		{
			//Iconify all frames.
			int result_width;
			
			if(getDesktopPanel() != null && getDesktopPanel().getAllFrames().length > 0)
			{
				result_width = getDesktopPanel().getWidth() / getDesktopPanel().getAllFrames().length;
			}else{
				result_width = getDesktopPanel().getWidth();
			}
			
			int result_height = getDesktopPanel().getHeight();
			
			int iteration = 0;
			for(JInternalFrame frame : getDesktopPanel().getAllFrames())
			{
				//Make all frames visible and deicony them.
				frame.setVisible(true);
				try {
					frame.setIcon(false);
				} catch (PropertyVetoException e1) {
					//NOOP
				}
				
				frame.setLocation(result_width * iteration, 0);
				frame.setSize(result_width, result_height);
				
				iteration++;
			}
			
			return;
		}
		
		/*
		 * Internal frame action event handling.
		 */
		if(e.getSource() instanceof JMenuItem)
		{
			JMenuItem menu_item = (JMenuItem) e.getSource();
			if(e.getActionCommand().compareTo("__FTDT_DEICONIFY") == 0)
			{
				for(Entry<JInternalFrame, JMenuItem> entry : getMenuWindowItems().entrySet())
				{
					if(entry.getValue() == menu_item)
					{
						JInternalFrame frame = entry.getKey();
						if(frame.isVisible())
						{
							frame.toFront();
							
							try {
								frame.setSelected(true);
							} catch (PropertyVetoException e1) {
								//Do nothing against the veto.
							}
							
							frame.requestFocusInWindow();
						}else{
							frame.setVisible(true);
							
							try {
								frame.setIcon(false);
								frame.setSelected(true);
							} catch (PropertyVetoException e1) {
								//Do nothing against the veto.
							}
							
							frame.requestFocusInWindow();
						}
						
						return;
					}
				}
			}
		}
	}

	public LogoDesktopPane getDesktopPanel() {
		return desktop_panel;
	}

	public JInternalFrame getActiveInternalFrame() {
		return active_internal_frame;
	}

	private void setActiveInternalFrame(JInternalFrame active_internal_frame) {
		this.active_internal_frame = active_internal_frame;
	}

	private void setDesktopPanel(LogoDesktopPane desktop_panel) {
		this.desktop_panel = desktop_panel;
	}

	public Component addInternalFrame(JInternalFrame frame) {
		if(frame != null)
		{
			frame.addComponentListener(this);
			frame.addInternalFrameListener(this);
			frame.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			
			JMenuItem menu_item = new JMenuItem(frame.getTitle());
			menu_item.setIcon(frame.getFrameIcon());
			menu_item.addActionListener(this);
			menu_item.setActionCommand("__FTDT_DEICONIFY");
			menu_window.add(menu_item);
			getMenuWindowItems().put(frame, menu_item);
		}
		
		//Make sure to make the windows status be properly updated.
		refreshWindowStatus();
		
		return desktop_panel.add(frame);
	}

	public JMenuItem getMenuIndicatorItem() {
		return menu_indicator_item;
	}

	protected void setMenuIndicatorItem(JMenuItem menu_indicator_item) {
		this.menu_indicator_item = menu_indicator_item;
	}

	public JMenu getMenuWindow() {
		return menu_window;
	}

	protected void setMenuWindow(JMenu menu_window) {
		this.menu_window = menu_window;
	}

	public HashMap<JInternalFrame, JMenuItem> getMenuWindowItems() {
		return menu_window_items;
	}
	
	public void removeWindowMenuItem(JInternalFrame frame)
	{
		getMenuWindow().remove(getMenuWindowItems().get(frame));
		getMenuWindowItems().remove(frame);
	}

	public JPanel getStatusBar() {
		return status_bar;
	}

	public void setStatusBar(JPanel status_bar) {
		this.status_bar = status_bar;
	}

	public BlockLibrary getFrameBlockLibrary() {
		return frame_block_library;
	}

	public void setFrameBlockLibrary(BlockLibrary frame_block_library) {
		this.frame_block_library = frame_block_library;
	}

	protected void refreshWindowStatus()
	{
		//Check how many frames that are alive.
		int frames = getDesktopPanel().getAllFrames().length;
		if(frames == 0)
		{
			//No frames? Add i a indicator disabled Menu Item.
			if(getMenuIndicatorItem() == null)
			{
				JMenuItem indicator = new JMenuItem("No Active Windows");
				setMenuIndicatorItem(indicator);
				getMenuWindow().add(indicator);
				indicator.setEnabled(false);
			}
			
			//Set the active frame to null for good measure.
			setActiveInternalFrame(null);
		}else{
			//Remove menu indicator.
			if(getMenuIndicatorItem() != null)
			{
				getMenuWindow().remove(getMenuIndicatorItem());
				setMenuIndicatorItem(null);
			}
		}
		
		refreshSaveStatus();
		refreshCloseStatus();
	}
	
	protected void refreshSaveStatus()
	{
		//Check for save capable frames.
		boolean can_save = false;
		
		for(JInternalFrame frame : getDesktopPanel().getAllFrames())
		{
			if(frame instanceof AbstractFtDInternalFrame)
			{
				AbstractFtDInternalFrame iframe = (AbstractFtDInternalFrame) frame;
				if(iframe.canSave(this));
				{
					can_save = true;
					break;
				}
			}
		}
		
		for(Component component : save_sensitive_components)
		{
			component.setEnabled(can_save);
		}
	}
	
	protected void refreshCloseStatus()
	{
		//Check for save capable frames.
		boolean status = false;
		
		if(getDesktopPanel().getAllFrames().length > 0)
		{
			status = true;
		}
		
		for(Component component : closeable_components)
		{
			component.setEnabled(status);
		}
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// NOOP
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// Very relevant!
		
		//Makes sure all frames always stays within the area.
		JInternalFrame[] frames = getDesktopPanel().getAllFrames();
		if(frames != null)
		{
			//Make sure we own the component.
			for(JInternalFrame frame : frames)
			{
				if(e.getSource() == frame)
				{
					//System.out.println("Stuff");
					
					//Check where it is and clamp it.
					Point position = frame.getLocation();
					Dimension size = frame.getSize();
					
					if(position.getX() < 0)
						position.x = 0;
					
					if(position.getY() < 0)
						position.y = 0;
					
					if(position.getX() + size.getWidth() > getDesktopPanel().getWidth())
						position.x = (int) (getDesktopPanel().getWidth() - size.getWidth());
					
					if(position.getY() + size.getHeight() > getDesktopPanel().getHeight())
						position.y = (int) (getDesktopPanel().getHeight() - size.getHeight());
					
					//Update new position
					frame.setLocation(position);
					
					//No need to loop further.
					break;
				}
			}
		}
	}

	@Override
	public void componentResized(ComponentEvent e) {
		// Maybe?
	}

	@Override
	public void componentShown(ComponentEvent e) {
		// NOOP
	}
}
