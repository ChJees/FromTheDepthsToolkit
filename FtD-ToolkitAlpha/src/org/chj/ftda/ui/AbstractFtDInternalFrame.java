package org.chj.ftda.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;

import org.chj.ftda.ToolkitAlpha;

/**
 * The base of all internal frames for the program. 
 * Can be anything from documents to blueprint renderers.
 * @author ChJees
 *
 */
public abstract class AbstractFtDInternalFrame extends JInternalFrame implements ActionListener {

	private static final long serialVersionUID = 5338048406402892251L;
	
	/**
	 * Creates a new internal frame and initializes it.
	 */
	public AbstractFtDInternalFrame() {
		baseInit();
	}
	
	/**
	 * Performs basic initialization procedure.
	 */
	private void baseInit()
	{
		//Initialize things common to all document frame.
		setFrameIcon(new ImageIcon(ToolkitAlpha.getIcons().get("page.png"))); //Set a default icon.
		setTitle("Untyped Frame"); //Set a default title.
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		//Document specific initialization
		init();
	}
	
	/**
	 * Initialization specific to this frame implementation.
	 */
	protected abstract void init();
	
	/**
	 * This frame is attempting to close. Returns true if it can be closed now.
	 * @param window The Toolkit Window which attempts to close.
	 * @return true if it can be closed.
	 */
	public abstract boolean canClose(ToolkitWindow window);
	
	/**
	 * Do this frame have the capability to save?
	 * @param window The Toolkit Window which owns this frame.
	 * @return true if it can save.
	 */
	public abstract boolean canSave(ToolkitWindow window);
	
	/**
	 * Frame is closing now. Do final cleanup here.
	 * @param window The Toolkit Window which attempted to close.
	 */
	public abstract void frameClosing(ToolkitWindow window);
	
	/**
	 * Frame attempts to close. 
	 * Perform appropriate action here like prompting to save and return true if it was successful.
	 * @param window The Toolkit Window which attempted to close.
	 * @return Success state. True mean the program can proceed with closing.
	 */
	public abstract boolean attemptToClose(ToolkitWindow window);
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//Do something important here.
	}
}
