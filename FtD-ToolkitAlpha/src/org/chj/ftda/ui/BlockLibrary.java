package org.chj.ftda.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.BevelBorder;
import javax.swing.tree.DefaultMutableTreeNode;

import org.chj.ftda.ToolkitAlpha;

/**
 * Frame intended for browsing all loaded blocks and 
 * display their name, UUID and statistic data. 
 * Later a block renderer will be added.
 * @author ChJees
 *
 */
public class BlockLibrary extends JFrame implements ActionListener {

	private static final long serialVersionUID = 7786971329533819585L;
	
	protected JTree item_list;
	
	public BlockLibrary() {
		init();
	}
	
	protected void init()
	{
		//Set attributes
		setAlwaysOnTop(true);
		setMinimumSize(new Dimension(400,300));
		setIconImage(ToolkitAlpha.getIcons().get("brick.png"));
		setTitle("Block Library");
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		
		//Add controls
		populate();
	}
	
	private void populate()
	{
		//Set layout
		setLayout(new GridBagLayout());
		
		//Add controls
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Test");
		
		JTree library_tree = new JTree(root);
		
		JScrollPane tree_pane = new JScrollPane(library_tree);
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets = new Insets(4, 4, 4, 4);
		//c.ipadx = 4;
		//c.ipady = 4;
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1.0d;
		c.weighty = 1.0d;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.BOTH;
		add(tree_pane, c);
		
		JPanel info_pane = new JPanel();
		info_pane.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 1;
		c.gridy = 0;
		c.weightx = 3.0d;
		c.weighty = 3.0d;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.BOTH;
		add(info_pane, c);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
