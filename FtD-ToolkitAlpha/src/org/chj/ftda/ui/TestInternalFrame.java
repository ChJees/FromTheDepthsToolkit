package org.chj.ftda.ui;

public class TestInternalFrame extends AbstractFtDInternalFrame {

	private static final long serialVersionUID = -3670510322377236075L;
	
	public TestInternalFrame() {
		//Herp
	}

	@Override
	protected void init() {
		//Derp
	}

	@Override
	public boolean canClose(ToolkitWindow window) {
		return true;
	}

	@Override
	public void frameClosing(ToolkitWindow window) {
		
	}

	@Override
	public boolean canSave(ToolkitWindow window) {
		return true;
	}

	@Override
	public boolean attemptToClose(ToolkitWindow window) {
		//JOptionPane.showMessageDialog(window, new JLabel("We are closing now."));
		return true;
	}
}
