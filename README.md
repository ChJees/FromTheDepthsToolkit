=From the Depths: Toolkit=

This program will let people get that little extra functionality out of their blueprints.
Unlike its predecessor this one is made with the ability to have several blueprints open
at once in mind.

==Libraries Used==

Jackson's Json
https://github.com/FasterXML/jackson


Core
http://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-core/2.7.2/


Databind
http://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.7.2/


Annotation
http://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.7.0/

JOGL
http://jogamp.org/jogl/www/

==Icons==

Mark James
http://www.famfamfam.com/lab/icons/silk/